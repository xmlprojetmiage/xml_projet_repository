<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:transform
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format"
xmlns:svg="http://www.w3.org/2000/svg"
xmlns:math="http://exslt.org/math"
exclude-result-prefixes="fo"
version="2.0"
>
<xsl:key name="regionValueByVal" match="ficheInscription/lieuRegion" use="."/>
<xsl:key name="themeValueByVal" match="ficheInscription/offres/offre/themes/theme" use="."/>
<xsl:template match="requete">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <fo:layout-master-set>
    <fo:simple-page-master master-name="my-page">
      <fo:region-body margin="1in"/>
    </fo:simple-page-master>
  </fo:layout-master-set>

			
  <fo:page-sequence master-reference="my-page">
    <fo:flow flow-name="xsl-region-body">
		    <fo:block font-size="25pt" text-align="center">Statistiques</fo:block>
			<xsl:variable name="nbFiche" select="(count(ficheInscription/lieuRegion))"/>
			<fo:block font-size="15pt" text-align="center" margin-top="10pt"><xsl:value-of select="'Lieu par r�gion'"/></fo:block>
			<fo:block></fo:block>
    		<fo:table>
    		
			  <fo:table-column column-width="5cm"/>
			  <fo:table-column column-width="10.2cm"/>
			  <fo:table-column column-width="2.5cm"/>
		 	
  			<fo:table-body font-size="10pt" font-family="sans-serif">
			 <xsl:for-each select="ficheInscription/lieuRegion[generate-id() = generate-id(key('regionValueByVal', .)[1])]">
		     	<xsl:variable name="width"  select="(round((count(key('regionValueByVal', .))) div $nbFiche *100*100) div 100 )*2.5"/>
		     	<xsl:variable name="width_rect" select="$width+20"/>
		     	<xsl:variable name="rgb1" select="round(math:random()*255)+100"/>
		     	<xsl:variable name="rgb2" select="round(math:random()*50)+150"/>
		     	<xsl:variable name="rgb3" select="round(math:random()*50)+20"/>
	    		<fo:table-row line-height="5pt">
	      			<fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
		      			<fo:block><xsl:value-of select="normalize-space(.)"/></fo:block>
	      			</fo:table-cell>
	      			<fo:table-cell>
		      			<fo:block>
			      			<fo:instream-foreign-object>
						     	<svg xmlns="http://www.w3.org/2000/svg" width="{$width_rect}" height="20" > -->
									<rect x="5" y="5" width="{$width}" height="20" style="fill:rgb({$rgb1},{$rgb2},{$rgb3});"/>
							    </svg>
						    </fo:instream-foreign-object>
					    </fo:block>
				    </fo:table-cell>
				    <fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
					    <fo:block>
					    	<xsl:value-of select="round((count(key('regionValueByVal', .))) div $nbFiche *100*100) div 100"/>% (<xsl:value-of select="count(key('regionValueByVal', .))"/>)
					    </fo:block>
				    </fo:table-cell>
				 </fo:table-row>
		     </xsl:for-each>
		     </fo:table-body>
		     </fo:table>
		     <xsl:variable name="nbTheme" select="(count(ficheInscription/offres/offre/themes/theme))"/>
			<fo:block font-size="15pt" text-align="center" margin-top="10pt"><xsl:value-of select="'Th�me des �venements'"/></fo:block>
			<fo:block></fo:block>
    		<fo:table>
    		
			  <fo:table-column column-width="5cm"/>
			  <fo:table-column column-width="10.2cm"/>
			  <fo:table-column column-width="2.5cm"/>
		 	
  			<fo:table-body font-size="10pt" font-family="sans-serif">
  			
			 <xsl:for-each select="ficheInscription/offres/offre/themes/theme[generate-id() = generate-id(key('themeValueByVal', .)[1])]">
		     	<xsl:variable name="width"  select="(round((count(key('themeValueByVal', .))) div $nbTheme *100*100) div 100 )*2.5"/>
		     	<xsl:variable name="width_rect" select="$width+20"/>
		     	
		     	<xsl:variable name="rgb1" select="round(math:random()*255)+100"/>
		     	<xsl:variable name="rgb2" select="round(math:random()*50)+150"/>
		     	<xsl:variable name="rgb3" select="round(math:random()*50)+20"/>
	    		<fo:table-row line-height="8pt">
	      			<fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
		      			<fo:block><xsl:value-of select="normalize-space(.)"/></fo:block>
	      			</fo:table-cell>
	      			<fo:table-cell>
		      			<fo:block>
			      			<fo:instream-foreign-object>
						     	<svg xmlns="http://www.w3.org/2000/svg" width="{$width_rect}" height="20" > -->
									<rect x="5" y="5" width="{$width}" height="20" style="fill:rgb({$rgb1},{$rgb2},{$rgb3});"/>
							    </svg>
						    </fo:instream-foreign-object>
					    </fo:block>
				    </fo:table-cell>
				    <fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
					    <fo:block>
					    	<xsl:value-of select="round((count(key('themeValueByVal', .))) div $nbTheme *100*100) div 100"/>% (<xsl:value-of select="count(key('themeValueByVal', .))"/>)
					    </fo:block>
				    </fo:table-cell>
				 </fo:table-row>
		     </xsl:for-each>
		     </fo:table-body>
		     </fo:table>
		     
			<fo:block font-size="15pt" text-align="center" margin-top="10pt">Type d&apos;�venements</fo:block>
			<fo:block></fo:block>
    		<fo:table>
    		
			  <fo:table-column column-width="5cm"/>
			  <fo:table-column column-width="10.2cm"/>
			  <fo:table-column column-width="2.5cm"/>
		 	
  			<fo:table-body font-size="10pt" font-family="sans-serif">
		     	<xsl:variable name="gratuit" select="count(ficheInscription/offres/offre[gratuit = 1])"/>
		     	<xsl:variable name="payant" select="count(ficheInscription/offres/offre[gratuit = 0])"/>
		     	<xsl:variable name="total" select="$gratuit+$payant"/>
		     	
		     	<xsl:variable name="gratuit_rect" select="$gratuit+20"/>
		     	<xsl:variable name="payant_rect" select="$payant+20"/>
		     	
		     	<xsl:variable name="gratuitGraph" select="round(($gratuit div $total)*100)"/>
		     	<xsl:variable name="payantGraph" select="round(($payant div $total)*100)"/>
	    		<fo:table-row line-height="5pt">
	      			<fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
		      			<fo:block><xsl:value-of select="'Gratuit'"/></fo:block>
	      			</fo:table-cell>
	      			<fo:table-cell>
		      			<fo:block>
			      			<fo:instream-foreign-object>
						     	<svg xmlns="http://www.w3.org/2000/svg" width="{$gratuit_rect*2.5}" height="20" >
									<rect x="5" y="5" width="{$gratuitGraph*2.5}" height="20" style="fill:rgb(255,69,0);"/>
							    </svg>
						    </fo:instream-foreign-object>
					    </fo:block>
				    </fo:table-cell>
				    <fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
					    <fo:block>
					    	<xsl:value-of select="$gratuitGraph"/>% (<xsl:value-of select="$gratuit"/>)
					    </fo:block>
				    </fo:table-cell>
				 </fo:table-row>
				 
	    		<fo:table-row line-height="5pt">
	      			<fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
		      			<fo:block><xsl:value-of select="'Payant'"/></fo:block>
	      			</fo:table-cell>
	      			<fo:table-cell>
		      			<fo:block>
			      			<fo:instream-foreign-object>
						     	<svg xmlns="http://www.w3.org/2000/svg" width="{$payant_rect*2.5}" height="20" >
									<rect x="5" y="5" width="{$payantGraph*2.5}" height="20" style="fill:rgb(107,142,35);"/>
							    </svg>
						    </fo:instream-foreign-object>
					    </fo:block>
				    </fo:table-cell>
				    <fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
					    <fo:block>
					    	<xsl:value-of select="$payantGraph"/>% (<xsl:value-of select="$payant"/>)
					    </fo:block>
				    </fo:table-cell>
				 </fo:table-row>
		     </fo:table-body>
		     </fo:table>
		     
		     <fo:block font-size="15pt" text-align="center" margin-top="10pt">Lieu</fo:block>
			<fo:block></fo:block>
    		<fo:table>
    		
			  <fo:table-column column-width="5cm"/>
			  <fo:table-column column-width="10.2cm"/>
			  <fo:table-column column-width="2.5cm"/>
		 	
  			<fo:table-body font-size="10pt" font-family="sans-serif">
		     	<xsl:variable name="cmh" select="count(ficheInscription[lieuClasseMH = 1])"/>
		     	<xsl:variable name="imh" select="count(ficheInscription[lieuInscritMH = 1])"/>
		     	<xsl:variable name="amh" select="count(ficheInscription[lieuInscritMH = 0 and lieuClasseMH = 0])"/>
		     	<xsl:variable name="total" select="$cmh+$imh+$amh"/>
		     	
		     	<xsl:variable name="cmh_rect" select="$cmh+20"/>
		     	<xsl:variable name="imh_rect" select="$imh+20"/>
		     	<xsl:variable name="amh_rect" select="$amh+20"/>
		     	
		     	<xsl:variable name="cmhGraph" select="round(($cmh div $total)*100)"/>
		     	<xsl:variable name="imhGraph" select="round(($imh div $total)*100)"/>
		     	<xsl:variable name="amhGraph" select="round(($amh div $total)*100)"/>
	    		<fo:table-row line-height="5pt">
	      			<fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
		      			<fo:block><xsl:value-of select="'Class� Monument Historique'"/></fo:block>
	      			</fo:table-cell>
	      			<fo:table-cell>
		      			<fo:block>
			      			<fo:instream-foreign-object>
						     	<svg xmlns="http://www.w3.org/2000/svg" width="{$cmh_rect*2.5}" height="20" >
									<rect x="5" y="5" width="{$cmhGraph*2.5}" height="20" style="fill:rgb(255,69,0);"/>
							    </svg>
						    </fo:instream-foreign-object>
					    </fo:block>
				    </fo:table-cell>
				    <fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
					    <fo:block>
					    	<xsl:value-of select="$cmhGraph"/>% (<xsl:value-of select="$cmh"/>)
					    </fo:block>
				    </fo:table-cell>
				 </fo:table-row>
				 
	    		<fo:table-row line-height="5pt">
	      			<fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
		      			<fo:block><xsl:value-of select="'Inscrit Monument Historique'"/></fo:block>
	      			</fo:table-cell>
	      			<fo:table-cell>
		      			<fo:block>
			      			<fo:instream-foreign-object>
						     	<svg xmlns="http://www.w3.org/2000/svg" width="{$imh_rect*2}" height="20" >
									<rect x="5" y="5" width="{$imhGraph*2}" height="20" style="fill:rgb(107,142,35);"/>
							    </svg>
						    </fo:instream-foreign-object>
					    </fo:block>
				    </fo:table-cell>
				    <fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
					    <fo:block>
					    	<xsl:value-of select="$imhGraph"/>% (<xsl:value-of select="$imh"/>)
					    </fo:block>
				    </fo:table-cell>
				 </fo:table-row>
				 
	    		<fo:table-row line-height="5pt">
	      			<fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
		      			<fo:block><xsl:value-of select="'Non Inscrit / Non Class�'"/></fo:block>
	      			</fo:table-cell>
	      			<fo:table-cell>
		      			<fo:block>
			      			<fo:instream-foreign-object>
						     	<svg xmlns="http://www.w3.org/2000/svg" width="{$amh_rect*2.5}" height="20" >
									<rect x="5" y="5" width="{$amhGraph*2.5}" height="20" style="fill:rgb(65,105,225);"/>
							    </svg>
						    </fo:instream-foreign-object>
					    </fo:block>
				    </fo:table-cell>
				    <fo:table-cell padding-before="3pt" padding-after="3pt" display-align="after">
					    <fo:block>
					    	<xsl:value-of select="$amhGraph"/>% (<xsl:value-of select="$amh"/>)
					    </fo:block>
				    </fo:table-cell>
				 </fo:table-row>
		     </fo:table-body>
		     </fo:table>
    </fo:flow>
  </fo:page-sequence>
</fo:root>
</xsl:template>
</xsl:transform>