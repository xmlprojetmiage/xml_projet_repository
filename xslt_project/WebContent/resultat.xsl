<?xml version="1.0" encoding="utf-8"?>
<xsl:transform
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT"
xmlns:js="http://saxonica.com/ns/globalJS"
xmlns:prop="http://saxonica.com/ns/html-property"
xmlns:style="http://saxonica.com/ns/html-style-property"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
exclude-result-prefixes="xs prop"
extension-element-prefixes="ixsl"
version="2.0"
>
<xsl:output method="html"
encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

<xsl:template name="reponse" match="/">
		<xsl:result-document href="#reponse" method="append-content"> 
		
			<div class="container requete">
				<div class="row">
					<xsl:if test="requete/ficheInscription">
						
					    <nav role="navigation" class="navBar">
					    	<div class="navig">
								<a href="index.html">Accueil</a>
								<a href="google_map.html">Carte</a>
								<a href="graphique.html">Statistiques</a>
							</div>
						</nav>
						<div class="useless"></div>
						
  						<h1 class="text-center">Résultat</h1>
					</xsl:if>
					<xsl:apply-templates select="requete/ficheInscription"/>
				</div>
			</div>
		</xsl:result-document>	
	</xsl:template>
	  	
<xsl:template match="ficheInscription">

		  <div class="col-xs-12 col-sm-12 col-md-12">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title"><xsl:value-of select="lieuNom"/></h3>
		  </div>
		  <div class="panel-body">
		  
			  <div class="col-xs-12 col-sm-12 col-md-12">
			<div class="panel panel panel-info">
			  <div class="panel-body" style="text-align:center;">
			  <strong>Adresse : </strong> <xsl:value-of select="lieuAdresse"/><br />
			  <strong>Ville : </strong> <xsl:value-of select="lieuCommune"/><br />
			  <strong>Code Postal : </strong> <xsl:value-of select="lieuCodePostal"/><br />
			  <strong>Numéro Insee : </strong> <xsl:value-of select="lieuNumInsee"/><br />
			  <strong>Région : </strong> <xsl:value-of select="lieuRegion"/><br />
			  <xsl:if test="lieuAcces">
			  	<strong>Accès : </strong> <xsl:value-of select="lieuAcces"/><br />
			  </xsl:if>
			  <xsl:if test="lieuAccesPartielHandicapes = 1">
			  	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Accès partiel handicappés<br />
			  </xsl:if>
			  <xsl:if test="lieuAccesTotalHandicapes = 1">
			  	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Accès total handicappés<br />
			  </xsl:if>
			  <xsl:if test="lieuClasseMH = 1">
			  	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Classé monument historique<br />
			  </xsl:if>
			  <xsl:if test="lieuInscritMH = 1">
			  	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Inscrit monument historique<br />
			  </xsl:if>
			</div>
		  	 </div>
				<xsl:apply-templates select="offres/offre"/>
		  	 </div>
			</div>
		  	 </div>
		</div>
</xsl:template>	  	
<xsl:template match="offre">

		  <div class="col-xs-12 col-sm-6 col-md-6">
		<div class="panel panel-info">
		  <div class="panel-heading">
		    <h3 class="panel-title"><xsl:value-of select="titre"/></h3>
		  </div>
		  <div class="panel-body">
		  <xsl:variable name="typeVisite">
		  	<xsl:value-of select="local-name(*[2])"/>
		  </xsl:variable>
		  <xsl:variable name="typeVisiteLength" select="string-length($typeVisite)-1"/>
		  <strong>Type de visite : </strong> <xsl:value-of select="substring($typeVisite,11,$typeVisiteLength)"/><br />
		  <strong>Dates : </strong> <xsl:value-of select="dateDebut"/> - 
		  <xsl:value-of select="dateFin"/><br />
		  <strong>Horraires : </strong> <xsl:value-of select="horaires"/><br />
		  <xsl:if test="conditions">
		  	<strong>Conditions : </strong> <xsl:value-of select="conditions"/><br />
		  </xsl:if>
		  <xsl:if test="gratuit = 1">
		  	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Gratuit
		  </xsl:if>
		  <xsl:if test="gratuit = 0">
		  	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Payant
		  </xsl:if>
		  <xsl:if test="inscriptionNecessaire = 1">
		   - Inscription nécessaire
		  </xsl:if>
		  <xsl:if test="surInvitation = 1">
		   - Sur invitation
		  </xsl:if>
		  <xsl:if test="themes/theme">
		  	<br /><strong>Thèmes : </strong> 	
		  </xsl:if>
	  	  <xsl:for-each select="themes/theme">
	  	  	<br /><xsl:value-of select="libelle"/>
	 	  </xsl:for-each>
		</div>
		</div>
		</div>
		  <xsl:variable name="offreNum">
		    <xsl:number level="single"/>
		  </xsl:variable>
		  <xsl:if test="$offreNum mod 2 = 0">
		  <div class="clear" style="clear: both;"></div>
		  </xsl:if>
</xsl:template>

</xsl:transform>