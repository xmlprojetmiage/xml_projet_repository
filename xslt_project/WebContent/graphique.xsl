<?xml version="1.0" encoding="utf-8"?>
<xsl:transform
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT"
xmlns:js="http://saxonica.com/ns/globalJS"
xmlns:prop="http://saxonica.com/ns/html-property"
xmlns:style="http://saxonica.com/ns/html-style-property"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:svg="http://www.w3.org/2000/svg"
exclude-result-prefixes="xs prop"
extension-element-prefixes="ixsl"
version="2.0"
>
<xsl:output method="html"
encoding="utf-8"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

<xsl:key name="regionValueByVal" match="requete/ficheInscription/lieuRegion" use="."/>
<xsl:key name="themeValueByVal" match="requete/ficheInscription/offres/offre/themes/theme" use="."/>

<xsl:template match="/">

<xsl:if test="requete/ficheInscription">

    <nav role="navigation" class="navBar">
    	<div class="navig">
			<a href="index.html">Accueil</a>
			<a href="resultat.html">Résultat</a>
			<a href="google_map.html">Carte</a>
		</div>
	</nav>
	<div class="useless"></div>
</xsl:if>
<div class="container requete">
	<div class="row">
		<xsl:if test="requete/ficheInscription">
		
		    <nav role="navigation" class="navBar">
		    	<div class="navig">
					<a href="index.html">Accueil</a>
					<a href="resultat.html">Résultat</a>
					<a href="google_map.html">Carte</a>
				</div>
			</nav>
			<div class="useless"></div>
			<div class="text-center" style="margin-bottom:15px;">
  				<h1 class="text-center">Statistiques</h1>
				<form id="pdf" action="XSLFOServlet" method="post">
				  <div class="text-center">
				  	<button type="submit" class="btn btn-default">Extraire un PDF</button>
				  </div>
				</form>
			</div>
		</xsl:if>
		  <div class="col-xs-12 col-sm-12 col-md-12">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title">Nombre de lieu par région</h3>
		  </div>
		  <div class="panel-body">
		  <div id="region_fiche"></div>
	     </div></div></div>
	     <div class="theme" style="display:none;">
	     <xsl:for-each select="requete/ficheInscription/offres/offre/themes/theme[generate-id() = generate-id(key('themeValueByVal', .)[1])]">
	     	<li class="titre"><xsl:value-of select="normalize-space(.)"/></li>
	     	<li class="valeur"><xsl:value-of select="count(key('themeValueByVal', .))"/></li>
	     </xsl:for-each>
	     </div>
		  <div class="col-xs-12 col-sm-12 col-md-12">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title">Thèmes des évenements</h3>
		  </div>
		  <div class="panel-body">
  		 <div id="theme_diag" class="circulaire"></div>
	     </div></div></div>
	     
	     <div class="offre" style="display:none;">
	     	<li class="gratuit"><xsl:value-of select="count(requete/ficheInscription/offres/offre[gratuit = 1])"/></li>
	     	<li class="payant"><xsl:value-of select="count(requete/ficheInscription/offres/offre[gratuit = 0])"/></li>
	     </div>
	     <div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title">Type d'évenement</h3>
		  </div>
		  <div class="panel-body">
  		 <div id="offre_diag" class="circulaire"></div>
	     </div></div></div>
	     
	     
	     <div class="mh" style="display:none;">
	     	<li class="cmh"><xsl:value-of select="count(requete/ficheInscription[lieuClasseMH = 1])"/></li>
	     	<li class="imh"><xsl:value-of select="count(requete/ficheInscription[lieuInscritMH = 1])"/></li>
	     	<li class="amh"><xsl:value-of select="count(requete/ficheInscription[lieuClasseMH = 0 and lieuInscritMH = 0])"/></li>
	     </div>
	     <div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-primary">
		  <div class="panel-heading">
		    <h3 class="panel-title">Lieu</h3>
		  </div>
		  <div class="panel-body">
  		 <div id="mh_diag" class="circulaire"></div>
	     </div></div></div>
		</div>
	</div>
	<script type="text/javascript"> 
	<![CDATA[
	
    	$(document).ready(function() {
		var themes_titre = [];
		var themes_valeur = [];
		var theme_tot = 0;

		$('.theme').each(function(){
		        $(this).find('.titre').each(function(){
		            var current = $(this);
		            if(current.children().size() > 0) {return true;}
		        	themes_titre.push(current.text());
		        });
		        $(this).find('.valeur').each(function(){
		            var current = $(this);
		            if(current.children().size() > 0) {return true;}
		        	themes_valeur.push(parseInt(current.text()));
		        	theme_tot += parseInt(current.text());
		        });
		    });
		    
		    createPieChart("theme_diag",themes_valeur,themes_titre,theme_tot);
		    
		var offre_titre = [];
		var offre_valeur = [];
		var offre_tot = 0;

		$('.offre').each(function(){
		        if(parseInt($(this).find('.gratuit').text()) > 0){
		        	offre_titre.push("Gratuit");
		        	offre_valeur.push(parseInt($(this).find('.gratuit').text()));
		        	offre_tot += parseInt($(this).find('.gratuit').text());
		        }
		        if(parseInt($(this).find('.payant').text()) > 0){
		        	offre_titre.push("Payant");
		        	offre_valeur.push(parseInt($(this).find('.payant').text()));
		        	offre_tot += parseInt($(this).find('.payant').text());
		        }
		    });
		    
		createPieChart("offre_diag",offre_valeur,offre_titre,offre_tot);  
		 
		var mh_titre = [];
		var mh_valeur = [];
		var mh_tot = 0;

		$('.mh').each(function(){
		        if(parseInt($(this).find('.cmh').text()) > 0){
		        	mh_titre.push("Classé Monument Historique");
		        	mh_valeur.push(parseInt($(this).find('.cmh').text()));
		        	mh_tot += parseInt($(this).find('.cmh').text());
		        }
		        if(parseInt($(this).find('.imh').text()) > 0){
		        	mh_titre.push("Inscrit Monument Historique");
		        	mh_valeur.push(parseInt($(this).find('.imh').text()));
		        	mh_tot += parseInt($(this).find('.imh').text());
		        }
		        if(parseInt($(this).find('.amh').text()) > 0){
		        	mh_titre.push("Non Classé / Non Inscrit");
		        	mh_valeur.push(parseInt($(this).find('.amh').text()));
		        	mh_tot += parseInt($(this).find('.amh').text());
		        }
		    });
		    
		createPieChart("mh_diag",mh_valeur,mh_titre,mh_tot);
		
		function createPieChart(nomID, valeur, titre, valeurTotal) {
		    var r = Raphael(nomID);
		
		    var pie = r.piechart(130, 120, 100, valeur, {legend: titre, legendpos: "east"});
		    pie.hover(
				// mouse over
		    	function () {
		        var that = this.sector;
		        this.sector.stop();
		        this.sector.scale(1.1, 1.1, this.cx, this.cy);
		
		        pie.each(function() {
		           if(this.sector.id === that.id) {
		            console.log(pie)
		               var valeurPercent = Math.round(this.sector.value.value/valeurTotal * 100);
		               tooltip = r.text(35, 220, valeurPercent+'%').attr({"font-size": 30, "fill":"#000"});
		           }
		        });
		
		        if (this.label) {
		            this.label[0].stop();
		            this.label[0].attr({ r: 7.5 });
		            this.label[1].attr({ "font-weight": 800 });
		        }
		    }, 
		
			// mouse out
		    function () {
		        this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
		        tooltip.remove();
		
		        if (this.label) {
		            this.label[0].animate({ r: 5 }, 500, "bounce");
		            this.label[1].attr({ "font-weight": 400 });
		        }
		    });
		}
		 var data;
	
		$.ajax({
			type : "GET",
			url : "test.xml",
			dataType : "xml",
			success : function(xml) {
				var region = {};
				var regionDensity;
				
				
				$(xml).find('ficheInscription').each(function() {
					try {
						region += "$" + $(this).find('lieuRegion').text();
					} catch (ex) {
						//If there is no region
					}
				});
				
				console.log(region.length)
				
				function calculateKeywordsDensity(phrase) {
					var words = phrase.split("$");
					var density = [];
					//sort the array
					words = words.sort(function (a, b) {
						if (a < b) return -1;
						if (a > b) return 1;
						return 0;
					});
					//used for store the word count
					var currentWordCount = 1;
					for (var i = words.length - 2; i >= 0; i--) {
						if (words[i] == words[i - 1]) {
							//a new duplicate keyword
							++currentWordCount;
						} else {
							//add the keyword with density to the array
							//console.log(i + words[i])
							density.push({
								word: words[i],
								count: currentWordCount
							});
							currentWordCount = 1;
						}
					}
					//sort the array with density of keywords
					density = density.sort(function (a, b) {
						if (a.count > b.count) return -1;
						if (a.count < b.count) return 1;
						return 0;
					});
					
					//var word = density[1].word.toString();
					//var count = density[1].count;
					
					return density;
				}
				
				//Extract values
				data = calculateKeywordsDensity(region);
				
				var margin = {
					top : 50,
					right : 40,
					bottom : 150,
					left : 40
				}, width = 960 - margin.left - margin.right, height = 500 - margin.top
						- margin.bottom;
			
				var formatPercent = d3.format(".0d");
			
				var x = d3.scale.ordinal().rangeRoundBands([ 0, width ], .1);
				var y = d3.scale.linear().range([ height, 0 ]);
				var xAxis = d3.svg.axis().scale(x).orient("bottom");
				var yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(formatPercent);
			
				var tip = d3.tip().attr('class', 'd3-tip').offset([ -10, 0 ]).html(
					function(d) {
						return "<strong>"+d.word+" : </strong> <span style='color:red'>" + d.count + "</span>";
					})
			
				var svg = d3.select("#region_fiche").append("svg").attr("width",
						width + margin.left + margin.right).attr("height",
						height + margin.top + margin.bottom).append("g").attr("transform",
						"translate(" + margin.left + "," + margin.top + ")");
			
				svg.call(tip);
			
				// The following code was contained in the callback function.
				x.domain(data.map(function(d) {
					return d.word;
				}));
				y.domain([ 0, d3.max(data, function(d) {
					return d.count;
				}) ]);
				
				svg.append("g")
					.attr("class", "x axis")
					.attr("transform","translate(0," + height + ")")
					.call(xAxis)
					.selectAll("text")  
					.style("text-anchor", "end")
					.attr("dx", "-.8em")
					.attr("dy", ".15em")
					.attr("transform", function(d) {
						return "rotate(-65)" 
					});;
			
				svg.append("g")
					.attr("class", "y axis")
					.call(yAxis)
					.append("text")
					.attr("transform", "rotate(-90)")
					.attr("y", 6)
					.attr("dy", ".25em")
					.style("text-anchor", "end")
					//.text("Frequency")
					;
			
				svg.selectAll(".bar").data(data).enter().append("rect")
						.attr("class", "bar").attr("x", function(d) {
							return x(d.word);
						}).attr("width", x.rangeBand()).attr("y", function(d) {
							return y(d.count);
						}).attr("height", function(d) {
							return height - y(d.count);
						}).on('mouseover', tip.show).on('mouseout', tip.hide)
			
				function type(d) {
					d.count = +d.count;
					return d;
				}
						}
					});
			});
		 ]]>
	</script>
</xsl:template>
</xsl:transform>