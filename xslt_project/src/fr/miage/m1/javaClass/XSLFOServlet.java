package fr.miage.m1.javaClass;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.basex.query.path.Test;

/**
 * Servlet implementation class XSLTServlet
 */
@WebServlet("/XSLFOServlet")
public class XSLFOServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FopFactory fopFactory = FopFactory.newInstance();
	private TransformerFactory tFactory = TransformerFactory.newInstance();


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public XSLFOServlet() {	
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Setup a buffer to obtain the content length
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		String workingDirectory = Test.class.getProtectionDomain().getCodeSource().getLocation().getFile().toString();
		System.out.println(workingDirectory);
		if(workingDirectory.contains("xml_projet_repository")){
			workingDirectory = workingDirectory.substring(1,workingDirectory.lastIndexOf("xml_projet_repository"));
		} else workingDirectory = workingDirectory.substring(1,workingDirectory.lastIndexOf(".metadata"));

		//Setup FOP
		Fop fop;
		try {
			fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);

		//Setup Transformer
		Source xsltSrc = new StreamSource(new File(workingDirectory+"xml_projet_repository\\xslt_project\\WebContent\\pdf.xsl"));
		Transformer transformer = tFactory.newTransformer(xsltSrc);

		//Make sure the XSL transformation's result is piped through to FOP
		Result res = new SAXResult(fop.getDefaultHandler());

		//Setup input
		Source src = new StreamSource(new File(workingDirectory+"xml_projet_repository\\xslt_project\\WebContent\\test.xml"));

		//Start the transformation and rendering process
		transformer.transform(src, res);

		} catch (FOPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Prepare response
		response.setContentType("application/pdf");
		response.setContentLength(out.size());
		response.setHeader("content-disposition: attachment", "inline; filename=\"Statistiques.pdf\"");

		//Send content to Browser
		response.getOutputStream().write(out.toByteArray());
		response.getOutputStream().flush();
	}
}
