package fr.miage.m1.javaClass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.basex.query.path.Test;

import fr.miage.m1.baseX.BaseXClient;

/**
 * Servlet implementation class XSLTServlet
 */
@WebServlet("/XSLTServlet")
public class XSLTServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public XSLTServlet() {	
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");

		final BaseXClient session = new BaseXClient("localhost", 1984, "admin", "admin");
		// ne pas oublier de run le server baseX de l'application database->Server administration

		try{
			
			String workingDirectory = Test.class.getProtectionDomain().getCodeSource().getLocation().getFile().toString();
			System.out.println(workingDirectory);
			if(workingDirectory.contains("xml_projet_repository")){
				workingDirectory = workingDirectory.substring(1,workingDirectory.lastIndexOf("xml_projet_repository"));
			} else workingDirectory = workingDirectory.substring(1,workingDirectory.lastIndexOf(".metadata"));

			//String path to the xml database file
			String jepXmlDir = Paths.get(workingDirectory+"xml_projet_repository\\xslt_project\\src\\fr\\miage\\m1\\baseX\\jep2014.xml").normalize().toString();
			//String path to the xml written file to use
			String requete = "";
			String requeteOffre = "";
			String reqFinal = "";
			int i = 0;
			if(request.getParameter("nomLieu") != "") {
				requete += "contains(lower-case(lieuNom),lower-case(\""+request.getParameter("nomLieu")+"\"))";
				i++;
			}
			if(request.getParameter("codeP") != "") {
				if(i > 0) {
					requete += " and ";
				}
				requete += "contains(lieuCodePostal,\""+request.getParameter("codeP")+"\")";
				i++;
			}
			if(request.getParameter("region") != "") {
				if(i > 0) {
					requete += " and ";
				}
				requete += "contains(lower-case(lieuRegion),lower-case(\""+request.getParameter("region")+"\"))";
				i++;
			}
			if(request.getParameter("commune") != "") {
				if(i > 0) {
					requete += " and ";
				}
				requete += "contains(lower-case(lieuCommune),lower-case(\""+request.getParameter("commune")+"\"))";
				i++;
			}
			if(request.getParameter("monumentH").equals("classe")) {
				if(i > 0) {
					requete += " and ";
				}
				requete += "lieuClasseMH = 1";
				i++;
			}
			if(request.getParameter("monumentH").equals("inscrit")) {
				if(i > 0) {
					requete += " and ";
				}
				requete += "lieuInscritMH = 1";
				i++;
			}
			if(request.getParameter("accesH").equals("partiel")) {
				if(i > 0) {
					requete += " and ";
				}
				requete += "lieuAccesPartielHandicapes = 1";
				i++;
			}
			if(request.getParameter("accesH").equals("total")) {
				if(i > 0) {
					requete += " and ";
				}
				requete += "lieuAccesTotalHandicapes = 1";
				i++;
			}
			
			
			if(request.getParameter("titre") != "") {
				if(i > 0) {
					requete += " and ";
				}
				requete += "contains(lower-case(offres/offre/titre),lower-case(\""+request.getParameter("titre")+"\"))";
				i++;
			}
			if(request.getParameter("date") != "") {
				if(i > 0) {
					requete += " and ";
				}
				System.out.println(request.getParameter("date"));
				String date = request.getParameter("date").substring(6,10)+request.getParameter("date").substring(3,5)+request.getParameter("date").substring(0,2);
				requete += "offres/offre/dateDebut[concat(substring(., 7, 4),substring(., 4, 2),substring(., 1, 2)) <= "+date+"] and offres/offre/dateFin[concat(substring(., 7, 4),substring(., 4, 2),substring(., 1, 2)) >= "+date+"]";
				i++;
			}
			if(request.getParameter("typeOffre").equals("gratuit")) {
				if(i > 0) {
					requete += " and ";
				}
				requete += "offres/offre/gratuit = 1";
				i++;
			}
			if(request.getParameter("typeOffre").equals("payant")) {
				if(i > 0) {
					requete += " and ";
				}
				requete += "offres/offre/gratuit = 0";
				i++;
			}
			if(!request.getParameter("typeVisite").equals("none")) {
				if(i > 0) {
					requete += " and ";
				}
				requete += "offres/offre/typeVisite"+request.getParameter("typeVisite")+" = 1";
				i++;
			}
			if(i > 0) {
				reqFinal = "["+requete+"]";
			}
			System.out.println(reqFinal);
			String testXmlDir = Paths.get(workingDirectory+"xml_projet_repository\\xslt_project\\WebContent\\test.xml").normalize().toString();
			

			createRequest(jepXmlDir,reqFinal,requeteOffre,testXmlDir,session);
			
		} finally {
			session.close();
		} 
		response.sendRedirect("resultat.html");
	}
	
	private boolean createRequest(String jepXmlDir, String reqFinal, String requeteOffre, String testXmlDir,BaseXClient session) throws IOException{
		String queryPath="";

		queryPath = "xquery doc('"+jepXmlDir+"')/programmeDataEdition/fichesInscription/ficheInscription"+reqFinal;
		//queryPath = "xquery doc('"+jepXmlDir+"')/programmeDataEdition/fichesInscription/ficheInscription/offres/offre/themes/theme/libelle";
		String req = session.execute(queryPath);
		//out.print(requete);
		String encodage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";			
		BufferedWriter output = new BufferedWriter(new PrintWriter(new File(testXmlDir), "UTF-8"));
		output.write(encodage+"\n<requete>\n"+req+"\n</requete>");
		output.flush();			
		output.close();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
		
	}
}
